export const swiperSlider = () => {
  var swiper = new Swiper(".kvSwiper", {
    slidesPerView: "auto",
    autoplay: true,
    freeMode: true,
    speed: 8000,
    centeredSlides: true,
    spaceBetween: 0,
    loop: true,
  });

  var swiper2 = new Swiper(".bannerSwiper", {
    slidesPerView: "auto",
    loop: true,
    centeredSlides: true,
    spaceBetween: 24,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
  });

  var swiper3 = new Swiper(".seminarSwiper", {
    slidesPerView: "auto",
    spaceBetween: 24,
    centeredSlides: true,
    navigation: {
      nextEl: ".seminar-button-next",
      prevEl: ".seminar-button-prev",
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    breakpoints: { 
      769: { 
        slidesPerView: 'auto', 
        spaceBetween: 24,
        centeredSlides: false
      } 
    },
  });
};
