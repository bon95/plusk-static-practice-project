export const loadMore = () => {
    $(document).ready(function(){

        var list = $(".js-newsItem");
        var numToShow = 5;
        var button = $(".js-newsBtn");
        var numInList = list.length;
        list.hide();
        if (numInList > numToShow) {
        button.show();
        }
        list.slice(0, numToShow).show();

        button.click(function(){
            var showing = list.filter(':visible').length;
            list.slice(showing - 1, showing + numToShow).fadeIn();
            var nowShowing = list.filter(':visible').length;
            if (nowShowing >= numInList) {
            button.hide();
            }
        });

    });

    $(document).ready(function(){

        var list = $(".js-caseItem");
        var numToShow = 6;
        var button = $(".js-caseBtn");
        var numInList = list.length;
        list.hide();
        if (numInList > numToShow) {
        button.show();
        }
        list.slice(0, numToShow).show();

        button.click(function(){
            var showing = list.filter(':visible').length;
            list.slice(showing - 1, showing + numToShow).fadeIn();
            var nowShowing = list.filter(':visible').length;
            if (nowShowing >= numInList) {
            button.hide();
            }
        });

    });

    $(document).ready(function(){

        var list = $(".js-faqItem");
        var numToShow = 5;
        var button = $(".js-faqBtn");
        var numInList = list.length;
        list.hide();
        if (numInList > numToShow) {
        button.show();
        }
        list.slice(0, numToShow).show();

        button.click(function(){
            var showing = list.filter(':visible').length;
            list.slice(showing - 1, showing + numToShow).fadeIn();
            var nowShowing = list.filter(':visible').length;
            if (nowShowing >= numInList) {
            button.hide();
            }
        });

    });

}